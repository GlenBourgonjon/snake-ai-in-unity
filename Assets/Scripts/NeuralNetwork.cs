﻿using System.Collections.Generic;
using System;
using System.IO;

// The neural network code is based on this tutorial / article:
// https://towardsdatascience.com/building-a-neural-network-framework-in-c-16ef56ce1fef

public class NeuralNetwork : IComparable<NeuralNetwork>
{
    private int m_Fitness = 0;
    private int m_AmountOfLayers = 0;
    private int[] m_Layers = null;
    private float[][] m_Neurons = null;
    private float[][] m_Biases = null;
    private float[][][] m_Weights = null;

    public NeuralNetwork( int[] layers )
    {
        InitLayers(layers);
        InitNeurons();
        InitBiases();
        InitWeights();
    }

    private void InitLayers( int[] layers )
    {
        // Create the layers.
        m_AmountOfLayers = layers.Length;
        m_Layers = new int[m_AmountOfLayers];
        for (int i = 0; i < m_AmountOfLayers; i++)
        {
            m_Layers[i] = layers[i];
        }
    }

    private void InitNeurons( )
    {
        // Create the neurons.
        List<float[]> tempNeurons = new List<float[]>();
        for (int i = 0; i < m_AmountOfLayers; ++i)
        {
            tempNeurons.Add(new float[m_Layers[i]]);
        }
        m_Neurons = tempNeurons.ToArray();
    }

    private void InitBiases( )
    {
        List<float[]> tempBiases = new List<float[]>();
        for (int i = 0; i < m_AmountOfLayers; ++i)
        {
            float[] bias = new float[m_Layers[i]];
            for (int j = 0; j < m_Layers[i]; ++j)
            {
                // All biases start with a random value between -0.5 and 0.5.
                bias[j] = UnityEngine.Random.Range(-0.5f, 0.5f);
            }
            tempBiases.Add(bias);
        }
        m_Biases = tempBiases.ToArray();
    }

    private void InitWeights( )
    {
        List<float[][]> tempWeights = new List<float[][]>();
        for (int i = 1; i < m_AmountOfLayers; ++i)
        {
            List<float[]> tempLayerWeights = new List<float[]>();
            int neuronsInPreviousLayer = m_Layers[i - 1];
            for (int j = 0; j < m_Neurons[i].Length; ++j)
            {
                float[] neuronWeights = new float[neuronsInPreviousLayer];
                for (int k = 0; k < neuronsInPreviousLayer; ++k)
                {
                    // All weights start with a random value between -0.5 and 0.5.
                    neuronWeights[k] = UnityEngine.Random.Range(-0.5f, 0.5f);
                }
                tempLayerWeights.Add(neuronWeights);
            }
            tempWeights.Add(tempLayerWeights.ToArray());
        }
        m_Weights = tempWeights.ToArray();
    }

    private float Activate( float value )
    {
        // Normalizing the value.
        value *= 5f;
        if (value < -60f)
            value = -60f;
        else if (value > 60f)
            value = 60f;
        return 1f / (1f + (float)Math.Exp(-value));
    }

    public float[] FeedForward( float[] inputs )
    {
        // Feeding the input to the first layer of neurons.
        for (int i = 0; i < inputs.Length; ++i)
        {
            m_Neurons[0][i] = inputs[i];
        }

        // For every layer after the first, do some magic.
        for (int i = 1; i < m_AmountOfLayers; ++i)
        {
            int layer = i - 1;
            for (int j = 0; j < m_Neurons[i].Length; ++j)
            {
                float value = 0f;
                for (int k = 0; k < m_Neurons[layer].Length; ++k)
                {
                    value += m_Weights[layer][j][k] * m_Neurons[layer][k];
                }
                m_Neurons[i][j] = Activate(value + m_Biases[i][j]);
            }
        }

        // Return the last layer.
        return m_Neurons[m_Neurons.Length - 1];
    }

    public void Mutate( int chance, float val )
    {
        for (int i = 0; i < m_Biases.Length; i++)
        {
            for (int j = 0; j < m_Biases[i].Length; j++)
            {
                m_Biases[i][j] = (UnityEngine.Random.Range(0f, chance) <= 5) ? m_Biases[i][j] + UnityEngine.Random.Range(-val, val) : m_Biases[i][j];
            }
        }

        for (int i = 0; i < m_Weights.Length; i++)
        {
            for (int j = 0; j < m_Weights[i].Length; j++)
            {
                for (int k = 0; k < m_Weights[i][j].Length; k++)
                {
                    m_Weights[i][j][k] = (UnityEngine.Random.Range(0f, chance) <= 5) ? m_Weights[i][j][k] + UnityEngine.Random.Range(-val, val) : m_Weights[i][j][k];
                }
            }
        }
    }

    public int CompareTo( NeuralNetwork other )
    {
        // Comparing the fitness of the two networks.
        if (other == null || m_Fitness > other.m_Fitness)
            return 1;
        else if (m_Fitness < other.m_Fitness)
            return -1;
        else
            return 0;
    }

    public NeuralNetwork Copy( NeuralNetwork network )
    {
        // Deep copy of the network.
        for (int i = 0; i < m_Biases.Length; ++i)
        {
            for (int j = 0; j < m_Biases[i].Length; ++j)
            {
                network.m_Biases[i][j] = m_Biases[i][j];
            }
        }
        for (int i = 0; i < m_Weights.Length; ++i)
        {
            for (int j = 0; j < m_Weights[i].Length; ++j)
            {
                for (int k = 0; k < m_Weights[i][j].Length; ++k)
                {
                    network.m_Weights[i][j][k] = m_Weights[i][j][k];
                }
            }
        }
        return network;
    }

    public void SetFitness( int fitness )
    {
        m_Fitness = fitness;
    }

    public void Load( string path )
    {
        // Load the file.
        TextReader tr = new StreamReader(path);
        int NumberOfLines = (int)new FileInfo(path).Length;
        string[] ListLines = new string[NumberOfLines];
        int index = 1;
        for (int i = 1; i < NumberOfLines; ++i)
        {
            ListLines[i] = tr.ReadLine();
        }
        tr.Close();

        // Set the weights and biases to the correct values.
        if (new FileInfo(path).Length > 0)
        {
            for (int i = 0; i < m_Biases.Length; ++i)
            {
                for (int j = 0; j < m_Biases[i].Length; ++j)
                {
                    m_Biases[i][j] = float.Parse(ListLines[index]);
                    ++index;
                }
            }
            for (int i = 0; i < m_Weights.Length; ++i)
            {
                for (int j = 0; j < m_Weights[i].Length; ++j)
                {
                    for (int k = 0; k < m_Weights[i][j].Length; ++k)
                    {
                        m_Weights[i][j][k] = float.Parse(ListLines[index]);
                        ++index;
                    }
                }
            }
        }
    }

    public void Save( string path )
    {
        // Create the file.
        File.Create(path).Close();
        StreamWriter writer = new StreamWriter(path, true);

        // Write the biases and weights to the file.
        for (int i = 0; i < m_Biases.Length; ++i)
        {
            for (int j = 0; j < m_Biases[i].Length; ++j)
            {
                writer.WriteLine(m_Biases[i][j]);
            }
        }
        for (int i = 0; i < m_Weights.Length; ++i)
        {
            for (int j = 0; j < m_Weights[i].Length; ++j)
            {
                for (int k = 0; k < m_Weights[i][j].Length; ++k)
                {
                    writer.WriteLine(m_Weights[i][j][k]);
                }
            }
        }
        writer.Close();
    }
}