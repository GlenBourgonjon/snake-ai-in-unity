﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Camera), typeof(SnakePlayer))]
public class CameraStuff : MonoBehaviour
{
    // This script was only used for the creation of a trailer video. It is irrelevant to the rest of the program.

    [SerializeField] private Vector3 m_StartPos = new Vector3();
    [SerializeField] private Vector3 m_EndPos = new Vector3();
    [SerializeField] private int m_StartSize = 1;
    [SerializeField] private int m_EndSize = 2;

    private bool m_Moving = false;
    private float m_Speed = 0.5f;

    private Camera m_Cam = null;

    private void Start( )
    {
        m_Cam = GetComponent<Camera>();
        m_Cam.orthographicSize = m_StartSize;

        transform.position = m_StartPos;
    }

    private void Update( )
    {
        if (!m_Moving && Input.GetKeyDown(KeyCode.Space))
        {
            m_Moving = true;
            StartCoroutine(Move());
        }
    }
    
    private IEnumerator Move( )
    {
        float t = 0f;
        while (t < 1f)
        {
            yield return new WaitForEndOfFrame();

            t += (m_Speed * Time.deltaTime);

            transform.position = Vector3.Lerp(m_StartPos, m_EndPos, t);
            m_Cam.orthographicSize = Mathf.Lerp(m_StartSize, m_EndSize, t);

            if (t >= 1f)
            {
                transform.position = m_EndPos;
                m_Cam.orthographicSize = m_EndSize;
            }
        }
    }
}
