﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class SnakePlayer : MonoBehaviour
{
    [SerializeField] private GameObject m_Prefab = null;
    [SerializeField] private int m_PopulationSize = 50;
    [SerializeField] [Range(0.0001f, 1f)] private float m_MutationChance = 0.01f;
    [SerializeField] [Range(0f, 1f)] private float m_MutationStrength = 0.5f;

    public float m_Gamespeed = 1f;

    [SerializeField] private Text m_IterationText = null;
    [SerializeField] private Text m_ScoreText = null;
    [SerializeField] private Text m_HighscoreText = null;

    private int m_Sessions = 0;

    private int[] m_Layers = new int[] { 4, 6, 6, 4 };
    private List<Snake> m_Games = new List<Snake>();
    private List<NeuralNetwork> m_Networks = new List<NeuralNetwork>();

    private List<float> m_Averages = new List<float>();
    private List<int> m_Highs = new List<int>();

    private bool m_WriteScores = false;
    private bool m_SaveNetwork = false;
    private bool m_LoadNetwork = false;
    private bool m_LoadedNetwork = false;

    private bool m_Ended = false;
    private float m_Delay = 1f;

    private bool m_WaitForAll = false;

    // Initialization.

    private void Start( )
    {
        InitNetworks();
        StartGame();
    }

    private void InitNetworks( )
    {
        m_Networks = new List<NeuralNetwork>(m_PopulationSize);
        for (int i = 0; i < m_PopulationSize; ++i)
        {
            NeuralNetwork network = new NeuralNetwork(m_Layers);
            m_Networks.Add(network);
        }
    }

    private void Update( )
    {
        if (Input.GetKeyDown(KeyCode.Return))
            m_WriteScores = true;

        if (Input.GetKeyDown(KeyCode.S))
            m_SaveNetwork = true;

        if (Input.GetKeyDown(KeyCode.L))
            m_LoadNetwork = true;

        if (Input.GetKeyDown(KeyCode.Space))
            m_WaitForAll = true;

        if (Input.GetKeyDown(KeyCode.P))
            m_Gamespeed = 180;

        // Checking if a new session should be started.
        if (!m_Ended && AllDead())
        {
            m_Ended = true;
            StartCoroutine(WaitToStart());
        }
    }

    private IEnumerator WaitToStart( )
    {
        yield return new WaitForSeconds(m_Delay);
        EndSession();
        if (m_WriteScores)
        {
            WriteScores("Assets/Resources/AverageAndHigh.txt");
            Debug.Log("Score data written to Assets/Resources/AverageAndHigh.txt.");
            m_WriteScores = false;
        }
        StartGame();
        m_Ended = false;
    }

    private void WriteScores( string path )
    {
        File.Create(path).Close();
        StreamWriter writer = new StreamWriter(path, false);

        // Write all of the averages and high scores to a file.
        for (int i = 0; i < m_Averages.Count; ++i)
        {
            writer.WriteLine(m_Averages[i] + ", " + m_Highs[i]);
        }

        writer.Close();
    }

    private bool AllDead( )
    {
        if (!m_WaitForAll && m_Games[90].IsDead())
            return true;

        foreach (Snake game in m_Games)
        {
            if (!game.IsDead())
                return false;
        }
        return true;
    }

    private void StartGame( )
    {
        int rows = Mathf.CeilToInt(Mathf.Sqrt(m_PopulationSize));
        float interval = 25f;

        if (m_LoadNetwork)
        {
            // Load the neural network that was saved to a file.
            for (int i = 0; i < m_PopulationSize; i++)
            {
                m_Networks[i].Load("BestNetwork.txt");
                // Half of the networks are the original saved network. The other half is mutated versions of that network.
                if (i < m_PopulationSize / 2)
                    m_Networks[i].Mutate((int)(1 / m_MutationChance), m_MutationStrength);
            }
            Debug.Log("Network data loaded from BestNetwork.txt.");
            m_LoadNetwork = false;
            m_LoadedNetwork = true;
            m_Sessions = 0;
            m_IterationText.text = "Current iteration after loading: 1";
            m_ScoreText.text = "";
            m_HighscoreText.text = "";
        }

        // Start the snake games and connect them to their own neural network.
        for (int i = 0; i < m_PopulationSize; i++)
        {
            Vector3 pos = new Vector3(interval * (i % rows), 0f, interval * (i / rows));
            Snake game = Instantiate(m_Prefab, pos, Quaternion.identity).GetComponent<Snake>();
            game.Init(m_Networks[i], m_Gamespeed);
            m_Games.Add(game);
        }
    }

    private void EndSession( )
    {
        ++m_Sessions;
        m_IterationText.text = "Current iteration" + (m_LoadedNetwork ? " after loading: " : ": ") + (m_Sessions + 1);

        // Sort the networks to determine which ones were the best.
        SortNetworks();
        if (m_SaveNetwork)
        {
            m_Networks[m_Networks.Count - 1].Save("Assets/Resources/BestNetwork.txt");
            Debug.Log("Network data saved to Assets/Resources/BestNetwork.txt.");
            m_SaveNetwork = false;
        }

        // Clear the current games.
        foreach (Snake game in m_Games)
        {
            Destroy(game.gameObject);
        }
        m_Games = new List<Snake>();
    }

    public void SortNetworks( )
    {
        // First let all games update the fitness of their neural network.
        int highscore = 0;
        int candy = 0;
        foreach (Snake game in m_Games)
        {
            game.UpdateFitness();
            int score = game.GetScore();
            if (highscore < score)
                highscore = score;
            candy += score;
        }

        // Record the average score and high score.
        m_Averages.Add((float)candy / m_Games.Count);
        m_Highs.Add(highscore);
        m_ScoreText.text = "Previous average score: " + ((float)candy / m_Games.Count);
        m_HighscoreText.text = "Previous high score: " + highscore;

        // Sort the networks based on their fitness.
        m_Networks.Sort();
        
        // The best half of the networks stays the same.
        // The worst half is discarded and replaced with mutations of the best half.
        for (int i = 0; i < m_PopulationSize / 2; ++i)
        {
            m_Networks[i] = m_Networks[i + m_PopulationSize / 2].Copy(new NeuralNetwork(m_Layers));
            m_Networks[i].Mutate((int)(1 / m_MutationChance), m_MutationStrength);
        }
    }
}
