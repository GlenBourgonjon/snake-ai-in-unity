﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Snake : MonoBehaviour
{
    // Neural network stuff
    private NeuralNetwork m_Network = null;
    private float[] m_Input = new float[4];

    // Game stuff
    private List<Vector2Int> m_Snake = new List<Vector2Int>();
    private List<Transform> m_SnakeObjects = new List<Transform>();
    private const int m_GridWidth = 21;
    private float m_Interval = 1f;
    private int m_MovesSinceLastCandy = 0;

    public enum Direction { right, up, left, down };
    private Direction m_LastDirection = Direction.up;

    // Score stuff
    private Vector2Int m_Candy = new Vector2Int();
    private Transform m_CandyObject = null;
    private int m_Score = 0;
    private bool m_Dead = false;
    private int m_CandyEaten = 0;

    // Prefabs
    [SerializeField] private GameObject m_SnakePrefab = null;
    [SerializeField] private GameObject m_CandyPrefab = null;

    [SerializeField] private GameObject m_Background = null;

    public void Init( NeuralNetwork network, float gameSpeed )
    {
        // Setting up stuff.
        m_Network = network;
        m_Interval = 1 / gameSpeed;
        m_Score = 0;

        // Spawning the snake.
        AddSnakePart(new Vector2Int(m_GridWidth / 2, m_GridWidth / 2));
        AddSnakePart(new Vector2Int(m_GridWidth / 2, m_GridWidth / 2 - 1));
        AddSnakePart(new Vector2Int(m_GridWidth / 2, m_GridWidth / 2 - 2));
        m_LastDirection = Direction.up;

        // Spawning the candy.
        m_CandyObject = Instantiate(m_CandyPrefab, transform).transform;
        SpawnCandy();

        // Start playing.
        StartCoroutine(Play());
    }

    public bool IsDead( )
    {
        return m_Dead;
    }

    public void UpdateFitness( )
    {
        m_Network.SetFitness(m_Score);
    }

    private IEnumerator Play( )
    {
        while (!m_Dead)
        {
            // Wait between every turn.
            yield return new WaitForSeconds(m_Interval);

            // First two inputs: the x and y coordinates of the snake head.
            m_Input[0] = (float)m_Snake[0].x / (m_GridWidth - 1);
            m_Input[1] = (float)m_Snake[0].y / (m_GridWidth - 1);
            // Last two inputs: the relative position of the candy, seen from the snake head.
            m_Input[2] = (float)(m_Candy.x - m_Snake[0].x) / (m_GridWidth - 1);
            m_Input[3] = (float)(m_Candy.y - m_Snake[0].y) / (m_GridWidth - 1);

            // Let the neural network do its thing.
            float[] output = m_Network.FeedForward(m_Input);

            // Interpret the output.
            float max = output[0];
            int idx = 0;
            for (int i = 1; i < output.Length; ++i)
            {
                if (output[i] > max)
                {
                    max = output[i];
                    idx = i;
                }
            }
            NewFrame((Direction)idx);

            // Die when the snake hasn't eaten for too long.
            if (m_MovesSinceLastCandy > 10 * m_GridWidth)
                Die();
        }
    }

    private void AddSnakePart( Vector2Int pos )
    {
        m_Snake.Add(pos);
        Transform newSnakePart = Instantiate(m_SnakePrefab, transform).transform;
        newSnakePart.localPosition = GridToWorld(pos);
        m_SnakeObjects.Add(newSnakePart);
    }

    private void SpawnCandy( )
    {
        // Make sure the candy doesn't spawn inside of the snake body.
        Vector2Int pos = new Vector2Int(Random.Range(0, m_GridWidth), Random.Range(0, m_GridWidth));
        while (m_Snake.Contains(pos))
        {
            pos = new Vector2Int(Random.Range(0, m_GridWidth), Random.Range(0, m_GridWidth));
        }
        m_Candy = pos;
        m_CandyObject.localPosition = GridToWorld(pos);
    }

    private Vector3 GridToWorld( Vector2Int pos )
    {
        return new Vector3(pos.x - m_GridWidth / 2, 0f, pos.y - m_GridWidth / 2);
    }

    public int GetScore( )
    {
        return m_CandyEaten;
    }

    private bool IsLegalPosition( Vector2Int pos )
    {
        // Is the snake head inside of the boundaries?
        return (pos.x < m_GridWidth && pos.y < m_GridWidth && pos.x >= 0 && pos.y >= 0);
    }

    private bool BitingOwnTail( )
    {
        // Is the snake head inside of its own body?
        for (int i = 1; i < m_Snake.Count; ++i)
        {
            if (m_Snake[i] == m_Snake[0])
                return true;
        }
        return false;
    }

    private void NewFrame( Direction direction )
    {
        // When trying to move in the opposite direction of the previous direction, don't listen.
        // Rather move into a random direction.
        while ((m_LastDirection == Direction.up && direction == Direction.down)
            || (m_LastDirection == Direction.down && direction == Direction.up)
            || (m_LastDirection == Direction.left && direction == Direction.right)
            || (m_LastDirection == Direction.right && direction == Direction.left))
            direction = (Direction)(Random.Range(0, 4));
        m_LastDirection = direction;

        // Move the snake head position.
        Vector2Int pos = m_Snake[0];
        switch (direction)
        {
            case Direction.right:
                ++pos.x;
                break;
            case Direction.up:
                ++pos.y;
                break;
            case Direction.left:
                --pos.x;
                break;
            case Direction.down:
                --pos.y;
                break;
        }

        // Remove the tail, and add a new snake part at the new position..
        Vector2Int lastPos = m_Snake[m_Snake.Count - 1];
        m_Snake.RemoveAt(m_Snake.Count - 1);
        m_Snake.Insert(0, pos);
        for (int i = 0; i < m_SnakeObjects.Count; ++i)
        {
            m_SnakeObjects[i].localPosition = GridToWorld(m_Snake[i]);
        }

        
        if (BitingOwnTail() || !IsLegalPosition(pos))
        {
            // Die when doing something dumb.
            Die();
            return;
        }
        else if (pos == m_Candy)
        {
            // Right now the snake doesn't get longer.
            //AddSnakePart(lastPos);

            // Get rewarded for eating candy.
            ++m_CandyEaten;
            m_Score += 10000;
            m_MovesSinceLastCandy = 0;
            SpawnCandy();
        }
        else
            ++m_MovesSinceLastCandy;
    }

    void Die( )
    {
        // The snake becomes red when dead.
        m_Dead = true;
        foreach (Transform t in m_SnakeObjects)
        {
            t.gameObject.GetComponent<Renderer>().material.color = Color.red;
        }

        m_Background.GetComponent<Renderer>().material.color = Color.gray;
    }
}
