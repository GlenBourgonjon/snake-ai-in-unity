# Snake AI in Unity

This project was my first step into the field of machine learning. I created a simple Snake game in Unity, and a basic neural network class. This class takes in a certain amount of inputs, pushes it through a certain amount of hidden layers, and spits out a certain amount of outputs. These hidden layers have weights and biases, and modify the data moving through them. After a lot of trial and error, I was able to make the game "understand" the rules of the game, without me explicitly explaining them.

My experiment ran with 100 Snake games at a time.
I tried giving the position of the snake on the playing field, and the relative position of the snake head to the candy, as input values to the network. I used the output of the network to choose if the snake should go a certain direction (up, down, right, left).
I then evaluate if a network does well. This means that it has good values for the weights and biases, so that it outputs a value that makes the snake move towards the candy in order to gain points.

​When all 100 snake games are over (because the snakes either moved out of the playing field or did not manage to collect candy for a long time), the best performing half of the networks are kept for the next iteration. The other half are thrown away, and replaced by mutations of the best half. This means that the weights and biases of those best networks are slightly modified.
After a lot of iterations, the networks really start to get good and have "taught" themselves to avoid the edges of the playing field and always move towards candy.

​Some stuff I would like to try out in the future:
- Right now, I select the fittest neural networks in a very crude way, by keeping the best performing half and replacing the worst performing half with mutations of the best performing half. Tuning this selection algorithm could help a lot in achieving results faster.
- Experimenting with different inputs, outputs and number of hidden layers could improve results as well.
- Right now, the snake doesn't grow bigger when eating candy. I'd like to experiment with this in the future to find out how this would affect the neural networks.